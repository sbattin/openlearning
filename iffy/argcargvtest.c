/** 
 * Author : Seth Battin
 * Date : 2012-10-30
 * Program : argcargvtest.c
 * Description: To test the usage of argc and argv.
 */

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int i;

    printf ("argc = %d\n", argc);
    for (i = 0; i < argc; i++){
        printf ("argv[%d] = '%s'\n", i, argv[i]);
    }

    return EXIT_SUCCESS;
}
