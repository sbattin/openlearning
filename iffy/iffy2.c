// program demonstrating the use if
// statements in c
// unsw computing 1 week 2
// www.openlearning.com/courses/computing1
 
#include <stdio.h>
#include <stdlib.h>
 
int main (int argc, char *argv[]) {
    int first;
    int second;
 
    if (argc > 1) {
        first = strtod(argv[1], NULL);
    } else {
        first = 3;
    }

    if (argc > 2) {
        second = strtod(argv[2], NULL);
    } else {
        second = 3;
    }

    for (second = 2; second < 12; second++){

        for (first = 0; first < 12; first++){


            //printf("\nUsing first=%d and second=%d\n", first, second);    

            if (first > 7) {
                printf ("Danish-");
                if (second < 3) {
                    printf ("happy");
                }
            } else {
                if (second > 8) {
                    printf ("Dan");
                } else if (second > 4) {
                    printf ("Sally");
                } else {
                    // print nothing
                }
            }
         
            if (second < 10) {
                printf ("ish-");
            }
         
            if (first > second) {
                printf ("-Flag-");
            } else {
                printf ("Flag-");
            }
            printf("\n");
        }
    }

 
    return EXIT_SUCCESS;
}
