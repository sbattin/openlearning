/*
 * Test if a point is inside a triangle.
 * Julian Saknussemm, de-confusinged by Seth Battin
 *
 * Given Three points of a triangle, and another arbitrary point this program determines if that point lies inside
 * the triangle.
 *
 * This is determined by satisfying the following rule:
 * A point P(x,y) is inside triangle A(x0,y0), B(x1,y1), C(x2,y2)
 * iff
 * P is on the same side of the line AB as C
 * P is on the same side of the line BC as A
 * and
 * P is on the same side of the line AC as B
 *
 * A special case exits for a vertical line (inf gradient) when testing the side of the line
 */

#include <stdlib.h> 
#include <stdio.h>
#include <assert.h>

#define POINT_LEFTSIDE -1
#define POINT_RIGHTSIDE 1
#define POINT_INLINE 0

#define TRUE 1
#define FALSE 0

int getPointSide(
   double test_x, double test_y, 
   double pointA_x, double pointA_y, 
   double pointB_x, double pointB_y);

int triangleContainsPoint(
   double A_x, double A_y, 
   double B_x, double B_y, 
   double C_x, double C_y, 
   double point_x, double point_y);

int main(int argc, char* argv[]) {
   
   
   assert(triangleContainsPoint(0,0,4,0,2,4,2,3) == TRUE);
   assert(triangleContainsPoint(0,0,4,0,2,4,5,3) == FALSE);
   assert(triangleContainsPoint(0,0,4,0,2,4,4,3) == FALSE);
   assert(triangleContainsPoint(0,0,4,0,2,4,3,4) == FALSE);
   assert(triangleContainsPoint(0,0,4,0,2,4,3,2) == FALSE);
   assert(triangleContainsPoint(0,0,4,0,2,4,3,-1) == FALSE);
   assert(triangleContainsPoint(0,0,4,0,2,4,3,1) == TRUE);
   
   assert(triangleContainsPoint(0,0,4,0,4,4,3,1) == TRUE); 
   
   printf("ALL TESTS PASSED!  YOU ARE AWESOME!\n");
   
   
   /*
   double A_x, A_y, B_x, B_y, C_x, C_y, point_x, point_y;
   int result;
   //get input
   printf("Input vertex A x-coordinate: "); 
   scanf("%lf", &A_x);
   printf("Input vertex A y-coordinate: ");
   scanf("%lf", &A_y);
   printf("Input vertex B x-coordinate: "); 
   scanf("%lf", &B_x);
   printf("Input vertex B y-coordinate: ");
   scanf("%lf", &B_y);
   printf("Input vertex C x-coordinate: "); 
   scanf("%lf", &C_x);
   printf("Input vertex C y-coordinate: ");
   scanf("%lf", &C_y);
   printf("Input test point x-coordinate: ");
   scanf("%lf", &point_x);
   printf("Input test point y-coordinate: ");
   scanf("%lf", &point_y);
   
   result = triangleContainsPoint(A_x, A_y, B_x, B_y, C_x, C_y, 
      point_x, point_y);
      
   printf ("Triangle (%.2lf,%.2lf)(%.2lf,%.2lf)(%.2lf,%.2lf) ", 
      A_x, A_y, B_x, B_y, C_x, C_y);
   
   if (result == TRUE) {
      printf ("DOES CONTAIN ");
   } else {
      printf ("DOES NOT CONTAIN ");
   }
   
   printf ("point (%.2lf,%.2lf).\n", point_x, point_y);
   
   */
   

   /*
   double x0,y0,x1,y1,x2,y2,px;
   double py;
    
   int scanfsReturnValueAggregatedOverAllScanfs = 0;
    
   // get input
           printf("Triangle Vertex A (x,y): "); scanfsReturnValueAggregatedOverAllScanfs += scanf("%lf,%lf", &x0,&y0);
           printf("Triangle Vertex  B (x,y): "); scanfsReturnValueAggregatedOverAllScanfs += scanf("%lf,%lf", &x1,&y1);
           printf("Triangle Vertex  C (x,y): "); scanfsReturnValueAggregatedOverAllScanfs += scanf("%lf,%lf", &x2,&y2);
      printf("Test Point (x,y): "); scanfsReturnValueAggregatedOverAllScanfs += scanf("%lf,%lf", &px,&py);
    
   // print error
      if( scanfsReturnValueAggregatedOverAllScanfs != 8 ) {
                   printf("You're stupid and didn't put in the right inputs!\n");
                   return 1;
   }
    
   // print answer
      printf("Point (%.2lf,%.2lf) is ", px,py);
      if(tritest(x0,y0,x1,y1,x2,y2,px,py)) printf("inside");
      else printf("outside");
      printf(" the Triangel\n");
    
   */
    
   // return EXIT_SUCCESS
   return EXIT_SUCCESS;
}
 
/* 
 * This function featured magic numbers for return values, so I replaced 
 * them with defined values.  It also used local variables with cryptic
 * names (albiet commonly used in the slope-intercept equation), so I 
 * replaced them with more descriptive names.  The formatting was just 
 * bad; not even close to our style guide.  It used multiple return 
 * statements, so I added a variable and then return it at the end. It 
 * also performed the same calculation twice, so I removed that code
 * duplication.
 * -Seth
 * P.S., I wasn't sure how to name the two values of a point, so I 
 * deviated from the style guide myself.  I think the subscripts it help
 * clearly describe them as two values from the same pair.
 * P.P.S, I think this function is wrong altogether.  The conversion to 
 * slope-intercept format never needs to happen, and adds a complication
 * to the case where the line is vertical.  I think I will refactor the 
 * rest of the code and deprecate this one.
 */
 
 /* deprecated.  Using raw point data now.
int getPointSide( double point_x, double point_y, double slope, double yAxisIntercept )
{   
   int side;
   double line_yAtPoint;
   line_yAtPoint = slope * point_x + yAxisIntercept;
   
   if( point_y < line_yAtPoint ) {
      side = POINT_LEFTSIDE; // point is under line
   } else if ( point_y == line_yAtPoint ){
      side = POINT_INLINE;
   } else {
      side = POINT_RIGHTSIDE; // over
   }
   
   return side;
   
}
*/

int getPointSide(
   double test_x, double test_y, 
   double pointA_x, double pointA_y, 
   double pointB_x, double pointB_y)
{
   
   int side;
   double slope;
   double intercept;
   double line_yAtTest_x;
   
   //make certain no two input points are equal
   assert( !((test_x == pointA_x) && (test_y == pointA_y)));
   assert( !((test_x == pointB_x) && (test_y == pointB_y)));
   assert( !((pointA_x == pointB_x) && (pointA_y == pointB_y)));
   
   if (pointA_x == pointB_x){ //vertical line.
      if (test_x == pointA_x){
         side = POINT_INLINE;
      } else if (pointA_y < pointB_y) { //upward line direction
         if (test_x < pointA_x) {
            side = POINT_RIGHTSIDE;  // right-hand rule
         } else {
            side = POINT_LEFTSIDE;
         }
      } else {
         if (test_x > pointA_x) {
            side = POINT_RIGHTSIDE;
         } else {
            side = POINT_LEFTSIDE;
         }
      }
   } else { //sloped line.  Use slope-intercept method
   
      // slope is change in y coordinate per change in x coordinate
      slope = (pointB_y - pointA_y) / (pointB_x - pointA_x);
      
      // intecept is the y coordinate of the line at x == 0;
      intercept = pointA_y - (slope * pointA_x);
      
      // find the height of the line at the x coordinate of the test
      line_yAtTest_x = intercept + slope * test_x;
      
      // check the test's y coordinate relative to that height
      if (line_yAtTest_x == test_y){
         side = POINT_INLINE;
      } else if (test_y > line_yAtTest_x){
         if (pointB_x > pointA_x) {
            side = POINT_RIGHTSIDE;
         } else {
            side = POINT_LEFTSIDE;         
         }
      } else {
         if (pointB_x > pointA_x) {
            side = POINT_LEFTSIDE;
         } else {
            side = POINT_RIGHTSIDE;
         }
      }      
   }
   
   return side;

}

int triangleContainsPoint(
   double A_x, double A_y, 
   double B_x, double B_y, 
   double C_x, double C_y, 
   double point_x, double point_y){

   int checkA, checkB, checkC, result;
   
   checkA = ( 
      getPointSide(A_x, A_y, B_x, B_y, C_x, C_y) ==
      getPointSide(point_x, point_y, B_x, B_y, C_x, C_y));
      
   checkB = (
      getPointSide(B_x, B_y, A_x, A_y, C_x, C_y) ==
      getPointSide(point_x, point_y, A_x, A_y, C_x, C_y));
      
   checkC = (
      getPointSide(C_x, C_y, A_x, A_y, B_x, B_y) == 
      getPointSide(point_x, point_y, A_x, A_y, B_x, B_y));
      
   result = ((checkA == TRUE) && (checkB == TRUE) && (checkC == TRUE));
   return result;
   
}
 /*
int // two points lie on the same side of a line
test1( double px, double py, double m,double b, double lx,double ly) {
        return (test2( px,py, m,b )==test2(lx,ly,m,b));
}
int tritest(double x0,double y0,double x1,double y1,double x2,double y2,double px, double py){
 
        int line1, line2, line3;
// line eqns
 
double m01 = (y1-y0)/(x1-x0);
// b: y - y1 = m( x - x1 ), x = 0
double b01 = m01 * -x1 + y1;
double m02, m12, b02, b12;
m02 = (y2-y0)/(x2-x0);
m12 = (y2-y1)/(x2-x1);
b02 = m02 * -x2 + y2;
b12 = m12 * -x2 + y2;
 
// vertical line checks
if( x1 == x0 ) {
   line1 = ( (px <= x0) == (x2 <= x0) );
} else {
   line1 = test1( px, py, m01, b01,x2,y2);
}
 
if( x1 == x2 ) {
        line2 = ( (px <= x2) == (x0 <= x2) );
} else {
        line2 = test1(px,py, m12, b12,x0,y0);
}
 
if( x2 == x0 ) {
line3 = ( (px <= x0 ) == (x1 <= x0) );} else {
        line3 = test1(px, py, m02,b02,x1,y1);
}
 
   return line1 && line2 && line3;
 
}
*/

