// leapyear function
// seth battin
// november 7 2012
// function to determine if a year is a leap your
// test case inputs: 1582, 1581, 1583, 1600, 1700, 2012, 2011, 4072, 6023480

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define START_OF_GREG_CALENDAR 1582
#define DAYS_IN_WEEK 7
#define LEAP_YEAR_FREQUENCY 4
#define LEAP_YEAR_CYCLE 400
#define YEARS_IN_CENTURY 100
#define IS_LEAPYEAR_TRUE 1
#define IS_LEAPYEAR_FALSE 0

int isLeapYear(int year);

int main(int argc, char * argv[]) {

    int year;

    printf ("please enter a year after %d\n", START_OF_GREG_CALENDAR);

    scanf("%d", &year);

    printf ("you entered %d\n", year);

    isLeapYear(year);

    return EXIT_SUCCESS;

}

int isLeapYear(int year){

    assert (year >= START_OF_GREG_CALENDAR);

    int result;

    if ((year % LEAP_YEAR_CYCLE) == 0){
        result = IS_LEAPYEAR_TRUE;

    } else if ((year % LEAP_YEAR_FREQUENCY) == 0){
        
        if ((year % YEARS_IN_CENTURY) == 0) {
            result = IS_LEAPYEAR_FALSE;
        } else {
            result = IS_LEAPYEAR_TRUE;
        }
    } else {
        result = IS_LEAPYEAR_FALSE;
    }

    if (result == IS_LEAPYEAR_TRUE){
        printf ("%d is a leap year!\n", year);
    } else {
        printf ("%d is not a leap year!\n", year);
    }


    return result;

}
