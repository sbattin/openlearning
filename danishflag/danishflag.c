/**
 * Author:      Seth Battin
 * Date:        2012-10-29
 * Program:     danishflag.c
 * Description: This program prints a 19x7 ASCII danish flag using "#" and " "
 */

#include <stdio.h>

int main(){
    const int maxx = 19;
    const int maxy = 7;
    const int yline = 3;
    const int xline1 = 5;
    const int xline2 = 6;
    int x;
    int y;

    for (y = 0; y < maxy; y++){
        if (y != yline) {
            for (x = 0; x < maxx; x++){
                if ((x == xline1) || (x == xline2)){
                    printf(" ");
                } else {
                    printf("#");
                }
            }
        }
        printf("\n");
    }
}
