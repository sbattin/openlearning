/*
 * Symmetrical.c
 * A program to accept an odd integer an print a symmetrical pattern.
 * Seth Battin
 * Feb 13th 2013
 */
 
#include <stdio.h>
#include <assert.h>

#define HASH "#"
#define DASH "-"

int main(int argc, char* argv[]) {
   int number, xcount, ycount;
   
   scanf("%d", &number);
   
   //thanks for the offer, but I will verify the number's oddness.
   if ((number % 2) == 1){
      
      xcount = 0;
      
      while (xcount < number){
      
         ycount = 0;
         
         while (ycount < number){
         
            if ((xcount == ycount) || ((xcount + ycount + 1) == number)){
               printf(HASH);
            } else {
               printf(DASH);
            }
            
            ycount++;
         }
         printf("\n");
      
         xcount++;
      }
   }
   
   return 0;
}
