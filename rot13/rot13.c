/*
 *  rot13.c
 *  Author: Seth Battin on Dec 9, 2012.
 *  Derived from encode.c, lecture code by Richard Buckland
 *  Licensed under Creative Commons SA-BY-NC 3.0.
 *
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
 
#define FIRST_LC_LETTER 'a'
#define LAST_LC_LETTER  'z'

#define FIRST_UC_LETTER 'A'
#define LAST_UC_LETTER 'Z'

#define ROTATION_NUMBER 13
 
void testEncode (void);
int isLowerCase (unsigned char letter);
int isUpperCase (unsigned char letter);
unsigned char encode (unsigned char letter);
 
int main (int argc, char *argv[]) {
 
   testEncode ();
 
   unsigned char plainChar;
   unsigned char encodedChar;
 
   printf ("Enter the letter to be encoded: ");
   scanf ("%c", &plainChar);
 
   encodedChar = encode(plainChar);

   printf ("Encoded is: %c\n", encodedChar);
 
   return EXIT_SUCCESS;
}
 
int isLowerCase (unsigned char letter) {
   return (letter >= FIRST_LC_LETTER) && (letter <= LAST_LC_LETTER);
}
int isUpperCase (unsigned char letter) {
   return (letter >= FIRST_UC_LETTER) && (letter <= LAST_UC_LETTER);
}
 
void testEncode (void) {

//   printf ("Test chars:\n");
//   printf (" Lowercase from %c to %c\n Uppercase from %c to %c\n",
//      FIRST_LC_LETTER, LAST_LC_LETTER, FIRST_UC_LETTER, LAST_UC_LETTER);
   
   
   
   assert (encode ('s') == 'f');
   
   assert (encode ('a') == 'n');
   assert (encode ('b') == 'o');
   assert (encode ('c') == 'p');
   assert (encode ('d') == 'q');
   assert (encode ('e') == 'r');
   assert (encode ('f') == 's');
   assert (encode ('g') == 't');
   assert (encode ('h') == 'u');
   assert (encode ('i') == 'v');
   assert (encode ('j') == 'w');
   assert (encode ('k') == 'x');
   assert (encode ('l') == 'y');
   assert (encode ('m') == 'z');
   
   assert (encode ('A') == 'N');
   assert (encode ('B') == 'O');
   assert (encode ('C') == 'P');
   assert (encode ('D') == 'Q');
   assert (encode ('E') == 'R');
   assert (encode ('F') == 'S');
   assert (encode ('G') == 'T');
   assert (encode ('H') == 'U');
   assert (encode ('I') == 'V');
   assert (encode ('J') == 'W');
   assert (encode ('K') == 'X');
   assert (encode ('L') == 'Y');
   assert (encode ('M') == 'Z');
   
   assert (encode (encode ('a')) == 'a'); // reversibility
   assert (encode ('n') == 'a');
   assert (encode ('A') == 'N');
   assert (encode ('N') == 'A');
   assert (encode ('7') == '7');
   
   // oops, while loops weren't available at the time of this assignment
   // get back up to date, and we can avoid this confusion, me.
   /*
   unsigned char testChar = 'a';
   while (testChar <= LAST_LC_LETTER){
   
//      printf("testing char '%c'...", testChar);
      assert(encode (encode (testChar)) == testChar);
//      printf("PASSED!\n");
      testChar += 1;
   }
   testChar = 'A';
   
   while (testChar <= LAST_UC_LETTER){
   
//      printf("testing char '%c'...", testChar);
      assert(encode (encode (testChar)) == testChar);
//      printf("PASSED!\n");
      testChar += 1;
   }
   
   testChar = '1';
   while (testChar <= '9')
   {
      assert(encode(testChar) == testChar);
      testChar += 1;
   }
   */
 
   printf ("All tests passed. You are Awesome!\n");
}
 
 
unsigned char encode (unsigned char letter) {
   unsigned char cipherText;
   int isLower = isLowerCase(letter);
   int isUpper = isUpperCase(letter);
 
   if (isLower) {
//      printf("'%c' is lowercase...", letter);
      cipherText = letter + ROTATION_NUMBER;
      if (cipherText > LAST_LC_LETTER) {
         cipherText -= (2 * ROTATION_NUMBER);
      }
 
   } else if (isUpper){
//      printf("'%c' is uppercase...", letter);
      cipherText = letter + ROTATION_NUMBER;
      if (cipherText > LAST_UC_LETTER) {
         cipherText -= (2 * ROTATION_NUMBER);
      }
   } else {
//      printf("'%c' is nothing to be encoded...", letter);
      cipherText = letter;
   }
   
   //printf("returning '%c' (which is '%i')...", cipherText, (int)cipherText);
   // AHHA!  SIGNED CHARS break at 's', on input 'f'!
    
   return cipherText;
}
