/*
 *  doomsday.c
 *  A program to test a function dayOfWeek which determines which
 *  day of the week a particular date falls on.
 *  (only for dates after the start of the Gregorian calendar).
 *
 *  Program stub created by Richard Buckland on 27/03/12
 *  This program by Seth Battin 28/11/12
 *  Freely licensed under Creative Commons CC-BY-3.0
 *
 *
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
 
#define THURSDAY 0
#define FRIDAY   1
#define SATURDAY 2
#define SUNDAY   3
#define MONDAY   4
#define TUESDAY  5
#define WEDNESDAY 6

#define JANUARY   1
#define FEBRUARY  2
#define MARCH     3
#define APRIL     4
#define MAY       5
#define JUNE      6
#define JULY      7
#define AUGUST    8
#define SEPTEMBER 9
#define OCTOBER   10
#define NOVEMBER  11
#define DECEMBER  12
 
#define TRUE 1
#define FALSE 0

#define ANCHOR_JAN 3
#define ANCHOR_FEB_AND_MAR 0

#define START_OF_GREG_CALENDAR 1582
#define DAYS_IN_WEEK 7
#define LEAP_YEAR_FREQUENCY 4
#define LEAP_YEAR_CYCLE 400
#define YEARS_IN_CENTURY 100
 
// Because the purpose of this exercise is the function dayOfWeek, and 
// the state nature of the main function, I elected to place the 
// function at the top.  Other functions still stubbed.
 
// given the doomsday for a year, and whether or not it is a
// leap year, this function return the day of the week for any
// given month and day in the year.


int dayOfWeek (int doomsday, int leapYear, int month, int day) {
   int dayOfWeek;
   int anchorDifference;
   
   // nix those big invalid values early
   day %= 7;
   
   if ((month < MARCH) && leapYear) {
      // Jan and Feb anchor days are increased in leap years.
      // That is effectively the same as decrimenting the doomsday
      doomsday = doomsday - 1;
   }
   
   if (month == JANUARY){
      anchorDifference = day - ANCHOR_JAN;
   } else if (month < APRIL){
      anchorDifference = day - ANCHOR_FEB_AND_MAR;
   
   } else if ((month % 2) == 0) {
      // even months (besides Feb) have anchor day equal to their own 
      // ordinal number
      anchorDifference = day - month;
      
   } else if ((month == MAY) || (month == SEPTEMBER)){
      // 5 and 9 have opposite anchors days
      anchorDifference = day - (MAY + SEPTEMBER - month);
      
   } else if ((month == JULY) || (month == NOVEMBER)){
      // 7 and 11 also have opposite anchor days
      anchorDifference = day - (JULY + NOVEMBER - month);
   }
   
   
   // to avoid modulus of negative numbers, add two weeks
   // http://stackoverflow.com/questions/7594508
   anchorDifference += (2 * DAYS_IN_WEEK); 
   
   
   
   
   // the distance from the doomsday, reduced the the 0-6 range
   dayOfWeek = (anchorDifference + doomsday) % DAYS_IN_WEEK;
   
   return (dayOfWeek);
}
 
int main (int argc, char *argv[]) {
   assert (dayOfWeek (THURSDAY,  FALSE,  MAY, 27) == MONDAY);
   assert (dayOfWeek (SATURDAY,  TRUE,   JANUARY, 17) == FRIDAY);
   assert (dayOfWeek (MONDAY,    TRUE,   FEBRUARY, 11) == THURSDAY);
   assert (dayOfWeek (SUNDAY,    FALSE,  FEBRUARY, 11) == THURSDAY);
 
   assert (dayOfWeek (THURSDAY,  FALSE,  APRIL,  4) == THURSDAY);
   assert (dayOfWeek (FRIDAY,    FALSE,  JUNE,  6) == FRIDAY);
   assert (dayOfWeek (MONDAY,    FALSE,  AUGUST,  8) == MONDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  OCTOBER, 10) == WEDNESDAY);
   assert (dayOfWeek (FRIDAY,    FALSE,  DECEMBER, 12) == FRIDAY);
   assert (dayOfWeek (THURSDAY,  FALSE,  SEPTEMBER,  5) == THURSDAY);
   assert (dayOfWeek (FRIDAY,    FALSE,  MAY,  9) == FRIDAY);
   assert (dayOfWeek (SUNDAY,    FALSE,  JULY, 11) == SUNDAY);
   assert (dayOfWeek (TUESDAY,   FALSE,  NOVEMBER,  7) == TUESDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  MARCH,  7) == WEDNESDAY);
 
   assert (dayOfWeek (THURSDAY,  FALSE,  APRIL,  5) == FRIDAY);
   assert (dayOfWeek (SATURDAY,  FALSE,  JUNE,  5) == FRIDAY);
   assert (dayOfWeek (MONDAY,    FALSE,  AUGUST,  9) == TUESDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  OCTOBER,  9) == TUESDAY);
   assert (dayOfWeek (FRIDAY,    FALSE,  DECEMBER, 20) == SATURDAY);
   assert (dayOfWeek (THURSDAY,  FALSE,  SEPTEMBER,  9) == MONDAY);
   assert (dayOfWeek (FRIDAY,    FALSE,  MAY,  5) == MONDAY);
   assert (dayOfWeek (SUNDAY,    FALSE,  JULY,  7) == WEDNESDAY);
   assert (dayOfWeek (TUESDAY,   FALSE,  NOVEMBER, 11) == SATURDAY);
   assert (dayOfWeek (THURSDAY,  FALSE,  MARCH, 30) == SATURDAY);
 
   assert (dayOfWeek (TUESDAY,   FALSE,  FEBRUARY, 28) == TUESDAY);
   assert (dayOfWeek (TUESDAY,   FALSE,  FEBRUARY, 27) == MONDAY);
   assert (dayOfWeek (THURSDAY,  FALSE,  JANUARY,  3) == THURSDAY);
 
// Additional tests
   assert (dayOfWeek (WEDNESDAY, TRUE,   JANUARY, 31) == TUESDAY);
   assert (dayOfWeek (MONDAY,    FALSE,  JANUARY, 31) == MONDAY);
 
   assert (dayOfWeek (TUESDAY,   TRUE,   JANUARY,  4) == TUESDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  JANUARY,  6) == SATURDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  DECEMBER, 25) == TUESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   FEBRUARY, 29) == WEDNESDAY);
   assert (dayOfWeek (THURSDAY,  TRUE,   FEBRUARY, 11) == SUNDAY);
 
   assert (dayOfWeek (MONDAY,    TRUE,   FEBRUARY, 29) == MONDAY);
   assert (dayOfWeek (MONDAY,    TRUE,   FEBRUARY, 28) == SUNDAY);
   assert (dayOfWeek (MONDAY,    TRUE,   FEBRUARY, 27) == SATURDAY);
   assert (dayOfWeek (MONDAY,    TRUE,   JANUARY,  3) == SUNDAY);
 
   assert (dayOfWeek (WEDNESDAY, TRUE,   JANUARY,  23) == MONDAY);
   assert (dayOfWeek (THURSDAY,  TRUE,   MARCH,  19) == TUESDAY);
 
   //Testing boundaries for different kind of year
   //year: 2000 - doomsday: TUESDAY - leap year
   assert (dayOfWeek (TUESDAY, TRUE,  JANUARY,  1) == SATURDAY);
   assert (dayOfWeek (TUESDAY, TRUE,  DECEMBER, 31) == SUNDAY);
   assert (dayOfWeek (TUESDAY, TRUE,  FEBRUARY, 29) == TUESDAY);
   assert (dayOfWeek (TUESDAY, TRUE,  MARCH,  1) == WEDNESDAY);
 
   //year: 1984 - doomsday: WEDNESDAY - leap year
   assert (dayOfWeek (WEDNESDAY, TRUE,  JANUARY,  1) == SUNDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  DECEMBER, 31) == MONDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  FEBRUARY, 29) == WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  MARCH,  1) == THURSDAY);
 
   //year: 1900 - doomsday: WEDNESDAY - non leap year
   assert (dayOfWeek (WEDNESDAY, FALSE,  JANUARY,  1) == MONDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  DECEMBER, 31) == MONDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  FEBRUARY, 28) == WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  MARCH,  1) == THURSDAY);
 
   //year: 2011 - doomsday: MONDAY - non leap year
   assert (dayOfWeek (MONDAY, FALSE,  JANUARY,  1) == SATURDAY);
   assert (dayOfWeek (MONDAY, FALSE,  DECEMBER, 31) == SATURDAY);
   assert (dayOfWeek (MONDAY, FALSE,  FEBRUARY, 28) == MONDAY);
   assert (dayOfWeek (MONDAY, FALSE,  MARCH,  1) == TUESDAY);
 
   //testing each month for a leap year (2012)
   assert (dayOfWeek (WEDNESDAY, TRUE,  JANUARY,  1) == SUNDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  FEBRUARY,  1) == WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  MARCH,  1) == THURSDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  APRIL,  1) == SUNDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  MAY,  1) == TUESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  JUNE,  1) == FRIDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  JULY,  1) == SUNDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  AUGUST,  1) == WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  SEPTEMBER,  1) == SATURDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  OCTOBER,  1) == MONDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  NOVEMBER,  1) == THURSDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,  DECEMBER,  1) == SATURDAY);
 
   //testing each month for a non leap year (2009)
   assert (dayOfWeek (SATURDAY, FALSE,  JANUARY,  1) == THURSDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  FEBRUARY,  1) == SUNDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  MARCH,  1) == SUNDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  APRIL,  1) == WEDNESDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  MAY,  1) == FRIDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  JUNE,  1) == MONDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  JULY,  1) == WEDNESDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  AUGUST,  1) == SATURDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  SEPTEMBER,  1) == TUESDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  OCTOBER,  1) == THURSDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  NOVEMBER,  1) == SUNDAY);
   assert (dayOfWeek (SATURDAY, FALSE,  DECEMBER,  1) == TUESDAY);
 
   //random various tests
   assert (dayOfWeek (MONDAY,   TRUE,  MARCH, 24) == THURSDAY);
   assert (dayOfWeek (MONDAY,   FALSE, MARCH, 24) == THURSDAY);
   assert (dayOfWeek (THURSDAY, FALSE, FEBRUARY, 17) == SUNDAY);
   assert (dayOfWeek (THURSDAY, TRUE,  FEBRUARY, 17) == SATURDAY);
   assert (dayOfWeek (SUNDAY,   TRUE,  NOVEMBER, 14) == SUNDAY);
   assert (dayOfWeek (SUNDAY,   TRUE,  FEBRUARY, 21) == SATURDAY);
   assert (dayOfWeek (MONDAY,   TRUE,  MAY,  7) == SATURDAY);
   assert (dayOfWeek (MONDAY,   FALSE, JULY,  5) == TUESDAY);
   assert (dayOfWeek (SATURDAY, FALSE, FEBRUARY, 28) == SATURDAY);
   assert (dayOfWeek (MONDAY,   TRUE,  FEBRUARY, 28) == SUNDAY);
   assert (dayOfWeek (MONDAY,   TRUE,  FEBRUARY, 29) == MONDAY);
   assert (dayOfWeek (MONDAY,   TRUE,  JANUARY,  3) == SUNDAY);
   assert (dayOfWeek (MONDAY,   TRUE,  JANUARY,  4) == MONDAY);
   assert (dayOfWeek (SATURDAY, FALSE, JANUARY,  3) == SATURDAY);
   assert (dayOfWeek (SATURDAY, FALSE, JANUARY,  4) == SUNDAY);
   assert (dayOfWeek (TUESDAY,  FALSE, JUNE, 6) == TUESDAY);
   assert (dayOfWeek (TUESDAY,  TRUE,  JANUARY, 31) == MONDAY);
   //different fix for the commented issue
   assert (dayOfWeek (WEDNESDAY, TRUE,  JANUARY,  6) == FRIDAY);
 
   // tests 2012
   assert (dayOfWeek(WEDNESDAY, TRUE, NOVEMBER, 16) == FRIDAY); // today
   assert (dayOfWeek(WEDNESDAY, TRUE, NOVEMBER, 17) == SATURDAY); // tomorrow
   assert (dayOfWeek(WEDNESDAY, TRUE, OCTOBER, 13) == SATURDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JUNE, 28) == THURSDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JANUARY, 8) == SUNDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JANUARY, 23) == MONDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, FEBRUARY, 9) == THURSDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, FEBRUARY, 14) == TUESDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, FEBRUARY, 26) == SUNDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, FEBRUARY, 28) == TUESDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, MARCH, 1) == THURSDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, MARCH, 5) == MONDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, APRIL, 6) == FRIDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, APRIL, 30) == MONDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, MAY, 9) == WEDNESDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, MAY, 18) == FRIDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, MAY, 28) == MONDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JUNE, 3) == SUNDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JUNE, 20) == WEDNESDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JUNE, 30) == SATURDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JULY, 3) == TUESDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JULY, 12) == THURSDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, JULY, 27) == FRIDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, AUGUST, 1) == WEDNESDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, AUGUST, 13) == MONDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, SEPTEMBER, 11) == TUESDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, SEPTEMBER, 24) == MONDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, OCTOBER, 11) == THURSDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, OCTOBER, 26) == FRIDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, NOVEMBER, 19) == MONDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, NOVEMBER, 24) == SATURDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, DECEMBER, 24) == MONDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, DECEMBER, 30) == SUNDAY);
 
   // 2011
   assert (dayOfWeek(MONDAY, FALSE, JANUARY, 12) == WEDNESDAY);
   assert (dayOfWeek(MONDAY, FALSE, FEBRUARY, 22) == TUESDAY);
   assert (dayOfWeek(MONDAY, FALSE, MARCH, 4) == FRIDAY);
   assert (dayOfWeek(MONDAY, FALSE, APRIL, 23) == SATURDAY);
   assert (dayOfWeek(MONDAY, FALSE, MAY, 8) == SUNDAY);
   assert (dayOfWeek(MONDAY, FALSE, JUNE, 17) == FRIDAY);
   assert (dayOfWeek(MONDAY, FALSE, JULY, 12) == TUESDAY);
   assert (dayOfWeek(MONDAY, FALSE, AUGUST, 7) == SUNDAY);
   assert (dayOfWeek(MONDAY, FALSE, SEPTEMBER, 27) == TUESDAY);
   assert (dayOfWeek(MONDAY, FALSE, OCTOBER, 5) == WEDNESDAY);
   assert (dayOfWeek(MONDAY, FALSE, NOVEMBER, 30) == WEDNESDAY);
   assert (dayOfWeek(MONDAY, FALSE, DECEMBER, 19) == MONDAY);
 
    // 2013
   assert (dayOfWeek(THURSDAY, FALSE, APRIL, 20) == SATURDAY);
   assert (dayOfWeek(THURSDAY, FALSE, FEBRUARY, 13) == WEDNESDAY);
 
   // contribution to the unit tests
   assert (dayOfWeek(SATURDAY, FALSE, OCTOBER, 9) == FRIDAY);
   assert (dayOfWeek(THURSDAY, FALSE, OCTOBER, 19) == SATURDAY);
   assert (dayOfWeek(MONDAY,   TRUE,  JANUARY, 1) == FRIDAY);
   assert (dayOfWeek(WEDNESDAY, TRUE, NOVEMBER, 15) == THURSDAY);
 
 
 
   // Contribution by AB. Random months and dates for all years starting from 2012 (at top) to 1900.
   // Calculated using Excel with correction for Excel's wrong leap year determination for 1900, the reason
   // for which this is interesting in itself)
 
   assert (dayOfWeek (WEDNESDAY, TRUE, DECEMBER, 19) ==  WEDNESDAY);
   assert (dayOfWeek (MONDAY,	   FALSE, MARCH, 9) ==  WEDNESDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, AUGUST, 26) ==  THURSDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, DECEMBER, 12) ==  SATURDAY);
   assert (dayOfWeek (FRIDAY,    TRUE, SEPTEMBER, 12) ==  FRIDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, DECEMBER, 29) ==  SATURDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, JUNE, 8) ==  THURSDAY);
   assert (dayOfWeek (MONDAY,    FALSE, SEPTEMBER, 31) ==  SATURDAY);
   assert (dayOfWeek (SUNDAY,    TRUE, OCTOBER, 26) ==  TUESDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, JULY, 19) ==  SATURDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, OCTOBER, 1) ==  TUESDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, AUGUST, 15) ==  WEDNESDAY);
   assert (dayOfWeek (TUESDAY,   TRUE, JULY, 21) ==  FRIDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, JULY, 18) ==  SUNDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, APRIL, 6) ==  MONDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, JUNE, 10) ==  TUESDAY);
   assert (dayOfWeek (THURSDAY,  TRUE, OCTOBER, 10) ==  THURSDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, JANUARY, 24) ==  TUESDAY);
   assert (dayOfWeek (MONDAY,    FALSE, MARCH, 17) ==  THURSDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, NOVEMBER, 19) ==  FRIDAY);
   assert (dayOfWeek (SATURDAY,  TRUE, DECEMBER, 12) ==  SATURDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, SEPTEMBER, 17) ==  TUESDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, JULY, 20) ==  FRIDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, DECEMBER, 29) ==  FRIDAY);
   assert (dayOfWeek (MONDAY,    TRUE, JANUARY, 8) ==  FRIDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, MARCH, 19) ==  THURSDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, MAY, 3) ==  SATURDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, AUGUST, 15) ==  THURSDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE, AUGUST, 17) ==  FRIDAY);
   assert (dayOfWeek (MONDAY,    FALSE, OCTOBER, 5) ==  WEDNESDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, FEBRUARY, 25) ==  THURSDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, APRIL, 8) ==  WEDNESDAY);
   assert (dayOfWeek (FRIDAY,    TRUE, JUNE, 2) ==  MONDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, OCTOBER, 28) ==  SUNDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, JULY, 9) ==  SUNDAY);
   assert (dayOfWeek (MONDAY,    FALSE, SEPTEMBER, 25) ==  SUNDAY);
   assert (dayOfWeek (SUNDAY,    TRUE, JULY, 3) ==  SATURDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, OCTOBER, 29) ==  WEDNESDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, FEBRUARY, 6) ==  WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, FEBRUARY, 1) ==  THURSDAY);
   assert (dayOfWeek (TUESDAY,   TRUE, JUNE, 16) ==  FRIDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, FEBRUARY, 20) ==  SATURDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, AUGUST, 17) ==  MONDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, MAY, 4) ==  SUNDAY);
   assert (dayOfWeek (THURSDAY,  TRUE, DECEMBER, 30) ==  MONDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, JULY, 1) ==  SATURDAY);
   assert (dayOfWeek (MONDAY,    FALSE, MAY, 25) ==  WEDNESDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, JULY, 14) ==  WEDNESDAY);
   assert (dayOfWeek (SATURDAY,  TRUE, DECEMBER, 7) ==  MONDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, FEBRUARY, 9) ==  SATURDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, FEBRUARY, 29) ==  THURSDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, OCTOBER, 1) ==  SUNDAY);
   assert (dayOfWeek (MONDAY,    TRUE, APRIL, 24) ==  SUNDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, OCTOBER, 20) ==  TUESDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, OCTOBER, 29) ==  WEDNESDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, MAY, 20) ==  MONDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE, SEPTEMBER, 1) ==  SATURDAY);
   assert (dayOfWeek (MONDAY,    FALSE, MAY, 9) ==  MONDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, MAY, 14) ==  FRIDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, APRIL, 29) ==  WEDNESDAY);
   assert (dayOfWeek (FRIDAY,    TRUE, APRIL, 26) ==  SATURDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, AUGUST, 9) ==  THURSDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, NOVEMBER, 18) ==  SATURDAY);
   assert (dayOfWeek (MONDAY,    FALSE, JANUARY, 8) ==  SATURDAY);
   assert (dayOfWeek (SUNDAY,    TRUE, JANUARY, 31) ==  SATURDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, MAY, 7) ==  WEDNESDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, JUNE, 3) ==  MONDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, MAY, 24) ==  THURSDAY);
   assert (dayOfWeek (TUESDAY,   TRUE, DECEMBER, 27) ==  WEDNESDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, JANUARY, 12) ==  TUESDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, OCTOBER, 27) ==  TUESDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, NOVEMBER, 25) ==  TUESDAY);
   assert (dayOfWeek (THURSDAY,  TRUE, FEBRUARY, 17) ==  SATURDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, MAY, 4) ==  THURSDAY);
   assert (dayOfWeek (MONDAY,    FALSE, JULY, 1) ==  FRIDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, NOVEMBER, 19) ==  FRIDAY);
   assert (dayOfWeek (SATURDAY,  TRUE, NOVEMBER, 1) ==  SUNDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, JANUARY, 28) ==  MONDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, AUGUST, 17) ==  FRIDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, NOVEMBER, 1) ==  WEDNESDAY);
   assert (dayOfWeek (MONDAY,    TRUE, NOVEMBER, 28) ==  MONDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, JULY, 3) ==  FRIDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, FEBRUARY, 18) ==  TUESDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, APRIL, 12) ==  FRIDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE, JUNE, 23) ==  SATURDAY);
   assert (dayOfWeek (MONDAY,    FALSE, MAY, 31) ==  TUESDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, MAY, 22) ==  SATURDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, AUGUST, 7) ==  FRIDAY);
   assert (dayOfWeek (FRIDAY,    TRUE, NOVEMBER, 9) ==  SUNDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, AUGUST, 7) ==  TUESDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, DECEMBER, 2) ==  SATURDAY);
   assert (dayOfWeek (MONDAY,    FALSE, MAY, 14) ==  SATURDAY);
   assert (dayOfWeek (SUNDAY,    TRUE, AUGUST, 14) ==  SATURDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, JUNE, 2) ==  MONDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, OCTOBER, 10) ==  THURSDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, MAY, 29) ==  TUESDAY);
   assert (dayOfWeek (TUESDAY,   TRUE, JULY, 15) ==  SATURDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, MAY, 29) ==  SATURDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, MAY, 28) ==  THURSDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, JANUARY, 7) ==  TUESDAY);
   assert (dayOfWeek (THURSDAY,  TRUE, OCTOBER, 4) ==  FRIDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, JUNE, 26) ==  MONDAY);
   assert (dayOfWeek (MONDAY,    FALSE, MAY, 2) ==  MONDAY);
   assert (dayOfWeek (SUNDAY,    FALSE, JANUARY, 31) ==  SUNDAY);
   assert (dayOfWeek (SATURDAY,  TRUE, AUGUST, 17) ==  MONDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, DECEMBER, 19) ==  THURSDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, AUGUST, 16) ==  THURSDAY);
   assert (dayOfWeek (TUESDAY,   FALSE, JUNE, 7) ==  WEDNESDAY);
   assert (dayOfWeek (MONDAY,    TRUE, JANUARY, 9) ==  SATURDAY);
   assert (dayOfWeek (SATURDAY,  FALSE, NOVEMBER, 18) ==  WEDNESDAY);
   assert (dayOfWeek (FRIDAY,    FALSE, JANUARY, 7) ==  TUESDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, NOVEMBER, 22) ==  FRIDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE, NOVEMBER, 11) ==  SUNDAY);
 
// Just to test peoples handling of neg numbers :D
   assert (dayOfWeek (THURSDAY,  FALSE, OCTOBER, 31) ==  THURSDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, NOVEMBER,  0) ==  THURSDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, NOVEMBER, -1) ==  WEDNESDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, NOVEMBER, -20) ==  FRIDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, JANUARY, -1) ==  SUNDAY);
   assert (dayOfWeek (THURSDAY,  TRUE, JANUARY, -1) ==  SATURDAY);
 
// year 1988 - doomsday is MONDAY
   assert (dayOfWeek (MONDAY,    TRUE,   JANUARY,  4) == MONDAY);
   assert (dayOfWeek (MONDAY,    TRUE,   FEBRUARY, 29) == MONDAY);
   assert (dayOfWeek (MONDAY,    TRUE,   APRIL, 16) == SATURDAY);
   assert (dayOfWeek (MONDAY,    TRUE,   MARCH,  7) == MONDAY);
 
// FALSE DATES (with true assertions)
   assert (dayOfWeek (MONDAY,	   FALSE,  FEBRUARY, 29) == TUESDAY);
   assert (dayOfWeek (WEDNESDAY, FALSE,  SEPTEMBER, 31) == MONDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   DECEMBER, 32) == TUESDAY);
 
   // First and last day of each month in 2012.
   assert (dayOfWeek (WEDNESDAY, TRUE,   JANUARY,  1)  ==  SUNDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   JANUARY,  31) ==  TUESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   FEBRUARY,  1)  ==  WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   FEBRUARY,  29) ==  WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   MARCH,  1)  ==  THURSDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   MARCH,  31) ==  SATURDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   APRIL,  1)  ==  SUNDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   APRIL,  30) ==  MONDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   MAY,  1)  ==  TUESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   MAY,  31) ==  THURSDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   JUNE,  1)  ==  FRIDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   JUNE,  30) ==  SATURDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   JULY,  1)  ==  SUNDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   JULY,  31) ==  TUESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   AUGUST,  1)  ==  WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   AUGUST,  31) ==  FRIDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   SEPTEMBER,  1)  ==  SATURDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   SEPTEMBER,  30) ==  SUNDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   OCTOBER, 1)  ==  MONDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   OCTOBER, 31) ==  WEDNESDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   NOVEMBER, 1)  ==  THURSDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   NOVEMBER, 30) ==  FRIDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   DECEMBER, 1)  ==  SATURDAY);
   assert (dayOfWeek (WEDNESDAY, TRUE,   DECEMBER, 31) ==  MONDAY);
 
   assert (dayOfWeek (FRIDAY,    TRUE,  JANUARY,  11) == FRIDAY);
 
   // Testing Christmas day of this year (2012) which is a leap
   // year and next year (2013) which is not a leap year
   assert (dayOfWeek (WEDNESDAY, TRUE,  DECEMBER, 25) == TUESDAY);
   assert (dayOfWeek (THURSDAY,  FALSE, DECEMBER, 25) == WEDNESDAY);
   
   // Test for today, and the next 500 days (every 10)
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 28) == THURSDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 38) == SUNDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 48) == WEDNESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 58) == SATURDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 68) == TUESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 78) == FRIDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 88) == MONDAY);
   
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 98) == THURSDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 108) == SUNDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 118) == WEDNESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 128) == SATURDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 138) == TUESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 148) == FRIDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 158) == MONDAY);
   
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 168) == THURSDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 178) == SUNDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 188) == WEDNESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 198) == SATURDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 208) == TUESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 218) == FRIDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 228) == MONDAY);
   
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 238) == THURSDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 248) == SUNDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 258) == WEDNESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 268) == SATURDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 278) == TUESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 288) == FRIDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 298) == MONDAY);
   
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 308) == THURSDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 318) == SUNDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 328) == WEDNESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 338) == SATURDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 348) == TUESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 358) == FRIDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 368) == MONDAY);
   
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 378) == THURSDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 388) == SUNDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 398) == WEDNESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 408) == SATURDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 418) == TUESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 428) == FRIDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 438) == MONDAY);
   
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 448) == THURSDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 458) == SUNDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 468) == WEDNESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 478) == SATURDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 488) == TUESDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 498) == FRIDAY);
   assert (dayOfWeek (THURSDAY, TRUE, NOVEMBER, 508) == MONDAY);
   
 
   printf ("all tests passed - You are awesome!\n");
   return EXIT_SUCCESS;
}

