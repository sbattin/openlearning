/*
 *  showdanish.c - prints the danish flag with while loops
 *  Author: Seth Battin on Dec 9, 2012.
 *  Licensed under Creative Commons SA-BY-NC 3.0.
 *
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define FLAG_HEIGHT 5
#define FLAG_WIDTH 12

#define SKIP_COLUMN 2
#define SKIP_ROW 2

void showDanish (void);
 
int main (int argc, char *argv[]) {

   showDanish();
   
   return EXIT_SUCCESS;

}

void showDanish(){

   int col = 0;
   int row = 0;
   
   while (row < FLAG_HEIGHT){
      col = 0;
      while (col < FLAG_WIDTH){
         if (row != SKIP_ROW){
            if (col != SKIP_COLUMN){
               printf("*");
            } else {
               printf(" ");
            }
         }
         col++;
      }
      printf("\n");
      row++;
   }
   

}
