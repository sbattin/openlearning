
#define POINTER_COUNT 14
#define TWO_TO_64_MINUS_ONE 18446744073709551615
#define TWO_TO_63_MINUS_ONE 9223372036854775807
#define TWO_TO_32_MINUS_ONE 4294967295
#define TWO_TO_31 2147483648
#define TWO_TO_16_MINUS_ONE 65535


	
long add (int x, int y);

void printSizes (
   unsigned long *ul, 
   unsigned int *ui, 
   unsigned long long *ull,
   unsigned short *us,
   signed long *sl,
   signed int *si,
   signed long long *sll,
   signed short *ss,
   long *l,
   int *i,
   long long *ll,
   short *s,
   float *f,
   double *d
);
   
void printAddresses (
   unsigned long *ul, 
   unsigned int *ui, 
   unsigned long long *ull,
   unsigned short *us,
   signed long *sl,
   signed int *si,
   signed long long *sll,
   signed short *ss,
   long *l,
   int *i,
   long long *ll,
   short *s,
   float *f,
   double *d
);

void sortAddresses (int addressCount, unsigned long addresses[]);

void bubbleAddresses (unsigned long *addr1, unsigned long *addr2);

void causeOverFlows(
   unsigned long ul, 
   unsigned int ui, 
   unsigned long long ull,
   unsigned short us,
   signed long sl,
   signed int si,
   signed long long sll,
   signed short ss,
   long l,
   int i,
   long long ll,
   short s,
   float f,
   double d
);
