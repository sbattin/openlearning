//
//  main.c
//  memory
//
//  Created by Seth Battin on 5/12/12
//  Stub by Richard Buckland on 20/11/12.
//
 
#include <stdio.h>
#include <stdlib.h>
#include "exploremem.h"
 
int main(int argc, const char * argv[]) {
 
 /*
   int x;
   int y;
   long total;
 
   x = 40;
   y = 2;
 
   total = add (x, y);
 
   printf("the sum of %d and %d is %ld\n", x, y, total);
   */
 
   char c ='a';
 
   unsigned long ul       = TWO_TO_64_MINUS_ONE;
   unsigned int ui        = TWO_TO_32_MINUS_ONE;
   unsigned long long ull = TWO_TO_64_MINUS_ONE;
   unsigned short us      = TWO_TO_16_MINUS_ONE;
 
   signed long sl       = 9223372036854775807ul;
   signed int si        = 5;
   signed long long sll = TWO_TO_63_MINUS_ONE;
   signed short ss      = 7;
 
   long l       = TWO_TO_63_MINUS_ONE;
   int i        = 9;
   long long ll = 10;
   short s      = 11;
 
   float f = 3.40282356779733642748073463979561713663e38;
   double d = 3.14;
   
//   printSizes(&ul, &ui, &ull, &us, 
//      &sl, &si, &sll, &ss, &l, &i, &ll, &s, &f, &d);
   
   printAddresses(&ul, &ui, &ull, &us, 
      &sl, &si, &sll, &ss, &l, &i, &ll, &s, &f, &d);
  
   
   causeOverFlows(ul, ui, ull, us, sl, si, sll, ss, l, i, ll, s, f, d);
      
   
   
 
   // add them all together just to make use of them so the compiler
   // doesn't grumble that they are unused in the program
 
   double grandTotal;
   grandTotal =  c +
                 ul + ui + ull + us +
                 sl + si + sll + ss +
                  l +  i +  ll +  s +
                  f + d;
 
   //printf ("all these things added together make %f\n", grandTotal);
 
 
   return EXIT_SUCCESS;
}
 
long add (int x, int y) {
   long answer;
   answer = x + y;
 
   return answer;
}
void printSizes (
   unsigned long *ul, 
   unsigned int *ui, 
   unsigned long long *ull,
   unsigned short *us,
   signed long *sl,
   signed int *si,
   signed long long *sll,
   signed short *ss,
   long *l,
   int *i,
   long long *ll,
   short *s,
   float *f,
   double *d
) {
   
   printf ("\nsizes:\n");
   printf (" ulong: %lu\n uint: %lu\n ulonglong: %lu\n ushort: %lu\n\n",
      sizeof(*ul), sizeof(*ui), sizeof(*ull), sizeof(*us));
   printf (" s-long: %lu\n s-int: %lu\n s-longlong: %lu\n s-short: %lu",
      sizeof(*sl), sizeof(*si), sizeof(*sll), sizeof(*ss));
   printf ("\n\n long: %lu\n int: %lu\n longlong: %lu\n short: %lu",
      sizeof(*l), sizeof(*i), sizeof(*ll), sizeof(*s));
   printf ("\n\n float: %lu\n double: %lu\n\n",
      sizeof(*f), sizeof(*d));
      
   
}


void printAddresses (
   unsigned long *ul, 
   unsigned int *ui, 
   unsigned long long *ull,
   unsigned short *us,
   signed long *sl,
   signed int *si,
   signed long long *sll,
   signed short *ss,
   long *l,
   int *i,
   long long *ll,
   short *s,
   float *f,
   double *d
) {
/*
   unsigned long pointers[POINTER_COUNT];
   
   pointers[0] = (unsigned long)ul;
   pointers[1] = (unsigned long)ui;
   pointers[2] = (unsigned long)ull;
   pointers[3] = (unsigned long)us;
   
   pointers[4] = (unsigned long)sl;
   pointers[5] = (unsigned long)si;
   pointers[6] = (unsigned long)sll;
   pointers[7] = (unsigned long)ss;
   
   pointers[8] = (unsigned long)l;
   pointers[9] = (unsigned long)i;
   pointers[10]= (unsigned long)ll;
   pointers[11]= (unsigned long)s;
   
   pointers[12] = (unsigned long)f;
   pointers[13] = (unsigned long)s;

*/
   printf ("addresses in order of declaration:\n");
   printf (" ulong: %p\n uint: %p\n ulonglong: %p\n ushort: %p\n\n",
      ul, ui, ull, us);
   printf (" s-long: %p\n s-int: %p\n s-longlong: %p\n s-short: %p",
      sl, si, sll, ss);
   printf ("\n\n long: %p\n int: %p\n longlong: %p\n short: %p",
      l, i, ll, s);
   printf ("\n\n float: %p\n double: %p\n\n",
      f, d);
/*
   sortAddresses(POINTER_COUNT, pointers);
  
   printf ("addresses in order of address:\n");
   printf (" ulong: %lx\n uint: %lx\n ulonglong: %lx\n ushort: %lx\n\n",
      pointers[0], pointers[1], pointers[2], pointers[3]);
   printf (" s-long: %p\n s-int: %p\n s-longlong: %p\n s-short: %p",
      sl, si, sll, ss);
   printf ("\n\n long: %p\n int: %p\n longlong: %p\n short: %p",
      l, i, ll, s);
   printf ("\n\n float: %p\n double: %p\n\n",
      f, d); 
      */

}

void sortAddresses (int addressCount, unsigned long addresses[]){
   int i, j;
   unsigned long addr1, addr2;
   
   i = 0;
   
   while (i < addressCount){
      j = i + 1;
      while (j < addressCount ){
         
         // Could I do this in one line?  Probably.
         addr1 = addresses[i];
         addr2 = addresses[j];
         
         bubbleAddresses(&addr1, &addr2);
         
         addresses[i] = addr1;
         addresses[j] = addr2;
         
         j++;
      }
      i++;
   }
   
   
   
}

void bubbleAddresses (unsigned long *addr1, unsigned long *addr2){
   unsigned long temp;
   if (addr1 > addr2){
      temp = *addr1;
      addr1 = addr2;
      *addr2 = temp;
   }
}

void causeOverFlows(
   unsigned long ul, 
   unsigned int ui, 
   unsigned long long ull,
   unsigned short us,
   signed long sl,
   signed int si,
   signed long long sll,
   signed short ss,
   long l,
   int i,
   long long ll,
   short s,
   float f,
   double d
){
   printf("unsigned long:          %lu\n", ul);
   printf("unsigned long + 1:      %lu\n", ul + 1);
   printf("unsigned long long:     %llu\n", ull);
   printf("unsigned long long + 1: %llu\n", ull + 1);
   printf("-1 as unsigned long:    %lu\n",0ul-1);
   if ((ul + 1) > ul){
      printf("max value + 1 is greater than max value\n");
   } else {
      printf("max value + 1 is less than max value\n");
   }
   
   printf("signed long long:     % lli\n", sll);
   printf("signed long long + 1: % lli\n", sll + 1);
   printf("signed long:          % li\n", sl);
   printf("signed long + 1:      % li\n", sl + 1);
   
   
   printf("unsigned int:          %u\n", ui);
   printf("unsigned int + 1:      %u\n", ui + 1);
   
   printf("long:                 % li\n", l);
   printf("long + 1:             % li\n", l + 1);
   
   printf("unsigned short:        %hu\n", us);
   printf("unsigned shoft + 1:    %hu\n", us + 1);
   
   printf("float:                % 16.48e\n", f);
   printf("float + 1:            % 16.48e\n", 1111111111111111111111111. + f);
   
}

