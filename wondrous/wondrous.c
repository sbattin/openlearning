/*
 * fancy flag - print out the fanciest possible flag using only whiles
 * created by Seth Battin, December 10th 2012
 * 
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int wondrous (int start);
 
int main (int argc, char *argv[]){

   int testvalue = 1000000000;
   int resultcount;
   int testcount;
   
   
   printf("input test upper limit (0 exits):");
   scanf("%i", &testcount);
   
   while (testvalue < testcount){
   
      // printf("testing sequence for %i:\n", testvalue);
      resultcount = wondrous(testvalue);
      //printf("sequence includes %i terms.\n", resultcount);
      
      if (resultcount > 1000) {
         printf("\n\n\nGOT A BIG ONE! (%i for value %i)\n\n\n\n\n", resultcount, testvalue);
         scanf("%i", &testvalue);
      }
      
      // printf("input test value (0 exits):");
      //scanf("%i", &testvalue);
      testvalue++;
      
   }
   
   printf("exiting with value %i\n", testvalue);


   return EXIT_SUCCESS;
}

int wondrous(int start) {

   // no overflow of an int, please.
   unsigned long longstart = (unsigned long)start;

   int termcount = 1;
   
   // assume that start is an integer greater than zero;
   
   while (longstart != 1){
   
      //printf("%lu ", longstart);
      
      termcount++;
      
      if ((longstart % 2) == 0){
         longstart /= 2;
      } else {
         longstart = (3 * longstart) + 1;
      }
   }
   
   //printf("%lu\n", longstart);
   
   return termcount;
   
}
