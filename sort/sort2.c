// sort function using swap
// seth battin
// december 10 2012
// function to sort three numbers usign ifs

#include <stdio.h>
#include <stdlib.h>

void swap (int * first, int * second);

int main(int argc, char * argv[]) {

   int a, b, c;

   printf("Please enter three whole numbers.  Press enter after each.\n");
   scanf("%d %d %d", &a, &b, &c);
   printf("You entered       %d %d %d\n", a, b, c);
   
   if (a > b){
      swap (&a, &b);
   }
   if (b > c){
      swap (&b, &c);
   }
   if (a > b){
      swap (&a, &b); 
   }

   printf("sorted, they are: %d %d %d\n", a, b, c);
   printf("%d\n%d\n%d\n", a, b, c);

   return EXIT_SUCCESS;
}

void swap (int * first, int * second){

   int temp = *first;
   *first = *second;
   *second = temp;

}
