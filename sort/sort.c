// sort function
// seth battin
// november 28 2012
// function to sort three numbers usign ifs

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {

    int a, b, c, first, second, third;

    //printf("Please enter three whole numbers.  Press enter after each.\n");
    scanf("%d %d %d", &a, &b, &c);

    //printf("You entered       %d %d %d\n", a, b, c);


    if (a > b) {

        if (b > c) {
            first = c;
            second = b;
            third = a;
        } else if (c > a) {
            first = b;
            second = a;
            third = c;
        } else {
            first = b;
            second = c;
            third = a;
        }

    } else if (a < b) {

        if (a > c) {
            first = c;
            second = a;
            third = b;
        } else if (c > b){
            first = a;
            second = b;
            third = c;
        } else {
            first = a;
            second = c;
            third = b;
        }

    } else {
        if (c > a) {
            first = a;
            second = b;
            third = c;
        } else {
            first = c;
            second = a;
            third = b;
        }
    }

    //printf("sorted, they are: %d %d %d\n", first, second, third);
    printf("%d\n%d\n%d\n", first, second, third);

    return EXIT_SUCCESS;
}

