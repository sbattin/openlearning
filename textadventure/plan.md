=Text adventure plan=
This document serves as the plan for creating a text adventure.  I am trying out markdown syntax for the first time (in source code).  
The game will utilize C as its language, using only the language features permitted in the openlearning.com class style guide.
==Features:==
* command line input
* config file parser (if file io is allowed)
* config string parser (if fio io is NOT allowed)
* config file interface (so the rest of the system doesn't care)
* player status/inventory
* linked list for nodes (rooms/areas/etc)
  
==Nodes==
Each node will contain all the information for a room, such as:
* Exits
* other description
* careful description (viewed by inspecting closely)
* items within
* searchable sub-rooms
* id value for references from other nodes

==Items==
* location
* description
* recipies...
* weight 
* uniqueness/stackability

==Technical research requirements==
There are some programming tehcniques that I will need to learn to use before I can complete this game.  Here is the list of C features over which I do not yet have sufficient mastery.
* Structs - need to learn to use more than simple field collections.
* Malloc - dynamic memory allocation and linked lists
* Pointers - Lots of reasons
I should note that all of these topics are covered in the content of the course, but their explanation is limited.  I may have to find some alternate reference material to really get a handle on them.  BUT, the course activities will give me a good way to exercise my research.

==Technical limitations==
I have written a fair bit of code prior to writing this section.  I realize now that there is one course limitation that is more restrictive than the others.  The absense of global variables of any kind.  The design implication is that all the data for the world must be passed into each function call that requires it.  Perhaps that is not such a terrible limitation...but it is cumbersome.  I *think* there are only two such structures that I need to pass around.  One for the player, and one for the environment.

==Story==
This is the big one.  What do I want to do in my adventure?  The idea of the "escape from room" game is interesting, but seems too cliche.  I could do something more abstract and odd...but I'm not sure how to go about that.  Driving in traffic?  No, that is beyond stupid.  Classic dungeon exploration might be good.  Perhaps not exactly classic.  aOOOHOHHHHH space themed.  Yes.  Or maybe not space themed directly...AH-ha!  I will tell a story I conceived of some time ago.  

The player awakes inside some sort of stasis chamber.  The player is wearing a light gas mask.  There are some instruments displaying vital information inside the chamber, and a red handle.

The chamber is in a windowless room on a submarine.  There is a single door which is sealed and barred from the player's side.  The room has a video display of hallway and a door which is barred from the other side.  There is a red LED number display which flashes 00:00:00.  There is a small chest which contains utility clothing and an unsealed manilla envelope.

I should pause here in my description of the story.  Obviously I will need to write a text adventure from the perspective of the player for the copy that goes into the game.  But I think I should not do that for my planning document here.  I should write out each component in each room in it's most descriptive form possible.  Let me begin again.

The game begins with the player inside a medical chamber.  This chamber is a statis device which stored the player's character until he was needed.  The theme of the game is that the player has become needed.  The player's purpose is to provide a failsafe on on AI controlled vessel (probably a submarine), where the failsafe is against the possibility that the AI becomes corrupt and threatens to use the vessel against humanity.  The statis chamber provides intraveinous nutrition and fluids, as well as oxygen.  The oxygen recycling system is removable from the statis chamber.  The player wears a bracelet which relays vital information to the medical device.  The medical device will display the player's current health whenever the player thinks to check it.

The room which contains the medical device contains a numerical wall clock (used as a timer) which has reduced to zero.  This timer reached zero because the vessel's captain ceased reseting its countdown when he ceased bein alive.  The timer reaching zero triggers the wake-up process for the player.

The room also contains a chest with utility clothes (the player starts wearing a medical gown).  The chest also contains mechanical and electrical tools, with equipment to carry them.  The chest also contains a manilla envelope which offers the player an explanation of who they are and what the player's purpose is.  The instructions tell the player that they must stop the evil computer, if only for the reason that the AI has killed the captain.  But also to prevetn a nuclear holocaust.

The room contains video screen which show the room's door from the outside, as well as the hallway leading away from the door.  These screens show nothing out of the ordinary.  Outside the door, there is no oxygen.  There is not an excess of carbon dioxide,s so the player will fall unconscious (permenantly) after only a fe wactions outside the door.  To avoid this, the player must use the oxygen recycling system in the medical device.  It can be removed by using the electrical and mechanical tools.

The computer will kill the player at the first opportunity, which it can accomplish in any number of way using all the mechanical and electrical equipment on the vessel.  The player must disable the computer in order to avoid being killed.  The computer's core is located in a position on the ship which the player cannot reach, so they must find the sub-floor passages to reach the computer's chamber.  Disabling it is trivial.

Once the computer is disabled, the player can reach any number of essentially random endings.  All the crew is dead, all the of the rest of humans are dead, the player successfully passes the virtual reality training course, or the player finds out that they are the AI, and they will be merged into the next military AI to improve it.

That's about it.  Not very many rooms, not very many endings, not very many interactions.  I think I can accomplish this without building an invincible content importing system and linked list node whatever whatever.  Here is what I absolutely need:

A player class (which will be a set of global variables or a struct) which denotes where the player is and what items the player has.  It must display various bits of information about objects.  The objects must know how they are retrieved, although this won't be a class method, it will be some logic or functions.

Really?  Is that it?  I think I need to try it out.
