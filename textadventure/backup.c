/**
 * Backup.c
 * A text adventure
 * Written by Seth Battin
 * March 22nd 2013
 */
 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

#define INPUT_MAX_LENGTH 128
#define INIT_OXYGEN 5

#define ROOM_MAX_ITEMS 8

#define CURSOR ">? "
#define COM_QUIT "quit"
#define COM_HELP "help"
#define COM_EMPTY ""
#define COM_HINT "hint"
#define COM_EXITS "exits"
#define COM_ITEMS "items"

#define COM_LOOK_0 "look"
#define COM_LOOK_1 "inspect"
#define COM_MOVE_0 "go"
#define COM_MOVE_1 "move"
#define COM_GET_0 "get"
#define COM_GET_1 "take"

#define LOC_CHAMBER 0
#define LOC_ROOM 1
#define LOC_HALLWAY 2
#define LOC_CRAWLSPACE 4

#define PASS_CHAMBER_ROOM 1
#define PASS_ROOM_HALLWAY 2
#define PASS_ROOM_CRAWLSPACE 4

#define LOC_CHAMBER_NAME "medical chamber"

#define TOTAL_OBJ 5
#define OBJ_BRACELET 0
#define OBJ_GOWN 1
#define OBJ_CLOTHES 2
#define OBJ_TOOLS 4
#define OBJ_MASK 8

#define OBJ_GOWN_NAME "medical gown"
#define OBJ_GOWN_DESC "A cheap, lightweight gown suitable for laying in a hospital bed.  It covers your body.  Mostly."
#define OBJ_CLOTHES_NAME "utility clothes"
#define OBJ_CLOTHES_DESC "A suit of durable and flexible clothing suitable for laboring. They have many pockets and straps and loops that allow carrying virtually unlimited items."
#define OBJ_TOOLS_NAME "set of tools"
#define OBJ_TOOLS_DESC "A variety of tools for all manner of mechnical and electrical maintenance. There are too many to carry."

typedef struct _player {
   int position;
   int oxygen;
   int clothes;
   unsigned long long items;
   unsigned long long openPassages;
} player;

typedef struct _room {
   int id;
   int items[ROOM_MAX_ITEMS];
} room;


void printTitle(void);
int updateStatus(char input[], player * the_player);
void printStatus(player the_player);
void confirmExit(char input[]);
void printHelp();
void printHint(int position, int items);
void printItems(int items, int clothes);
void printExits(player the_player);
void printPlayerHas(int item, int clothes);
int recognizeVerb(char * word);
int recognizeRoomAction(char * word, player the_player) 
void parseVerb(char * word);

int main (int argc, char *argv[]) {
   
   player the_player;
   the_player.position = 0;
   the_player.items = OBJ_BRACELET | OBJ_GOWN;
   the_player.openPassages = 0;
   the_player.oxygen = INIT_OXYGEN;
   the_player.clothes = OBJ_GOWN;
   
   char input[INPUT_MAX_LENGTH];
   int i;
   
   i = 0;
   while (i < INPUT_MAX_LENGTH){
      input[i] = 0;
      i++;
   }
   
   //set position from command line args?
   if (the_player.position == 0){
      printTitle();
      printStatus(the_player);
   }
   
   while (strcmp(input, COM_QUIT) != 0){
      printf   ("\n%s", CURSOR);
      gets(input);
      if (updateStatus(input, &the_player)){   
         printStatus(the_player);
      }
   }
   
   return EXIT_SUCCESS;
}

void printTitle(void) {
   
   printf("Welcome to...\n"
      "BACKUP - a text adventure.\n"
      "\n"
      "To get help, type '%s'.\n"
      "To play, try the usual commands like '%s', '%s', or '%s'.\n"
      "\n",
      COM_LOOK_0, COM_GET_0, COM_HELP, COM_MOVE_0);
      
}

int updateStatus(char input[], player * the_player){
   
   int newStatus = FALSE;
   int parsing = TRUE;
   char * ptr;   
   // check input special cases first
   if (strcmp(input, COM_QUIT) == 0){
   
      confirmExit(input);
      
   } else if (strcmp(input, COM_HELP) == 0) {
   
      printHelp();
      
   } else if (strcmp(input, COM_HINT) == 0) {
   
      printHint(the_player->position, the_player->items);
      
   } else if (strcmp(input, COM_ITEMS) == 0) {
   
      printItems(the_player->items, the_player->clothes);
   
   } else if (strcmp(input, COM_EXITS) == 0) {
   
      printExits(*the_player);
   
   } else if (strcmp(input, COM_EMPTY) == 0) {
   
      // do nothing;
   
   } else { // otherwise, do parsing
   
      newStatus = TRUE;
      
      ptr = strtok(input, ",.-; ");
      
      while (parsing && (ptr != NULL)){
      
         if (recognizeVerb(ptr *the_player)){
            parseVerb(ptr, *the_player);
            parsing = FALSE;
         } else {
            ptr = strtok(NULL, ",.-; ");
         }
      
      }
   
   }
   
   return newStatus;
}


void printStatus(player the_player){
   if (the_player.position == 0){
      if (the_player.openPassages & PASS_CHAMBER_ROOM){
         printf("You are laying the medical chamber. ");
      } else {
         printf("You open your eyes to see the inside of glass, coffin-"
         "like chamber. ");
      }
      printf("There is a slight electric hum. "
         "You are wearing a loose mask that has some flow of gas when "
         "you inhale. There are a variety of displays which seem to "
         "display health vital information.");
      if (the_player.openPassages & PASS_CHAMBER_ROOM){
         printf(" The glass cover is opened.");
      } else {
         printf(" There is a red handle at the end of the chamber " 
            "above your head.");
      }
   } else if (the_player.position == 1){
      printf("The room holding the medical chamber is barely large "
         "enough to contain it.  There is a single shelf, on which "
         "sits a laminated document");
      if (!(the_player.items & OBJ_CLOTHES) && 
         !(the_player.items & OBJ_TOOLS)){
         printf(", %s, and %s.", OBJ_CLOTHES_NAME, OBJ_TOOLS_NAME);
      } else if (!(the_player.items & OBJ_CLOTHES)) {
         printf("and %s.", OBJ_CLOTHES_NAME);
      } else if (!(the_player.items & OBJ_TOOLS)) {
         printf("and %s.", OBJ_TOOLS_NAME);
      } else {
         printf(".");
      }
      
   }
}

void confirmExit(char input[]) {
   
   printf("Really? Type '%s' again to exit the game,"
      " or press enter to cancel.\n", COM_QUIT);
   
   gets(input);
   
   if(strcmp(input, COM_QUIT) == 0){
      printf("Ok, bye.\n");
   }
}

void printHelp(){
   
   printf("\nThis is the help.  You can refer to it at any time.\n\n");
   printf("This page does not give hints about what to do to win "
      "the game.  To get a tip on what to do next, type "
      "'%s'.\n\n", COM_HINT);
   printf("To exit the game, type '%s'.  You will need to type it twice"
      " to confirm.\n\n", COM_QUIT);
   printf("To see your surroundings, type '%s' or '%s'.  To inspect something, "
      "type '%s {something}'.", COM_LOOK_0, COM_LOOK_1, COM_LOOK_0);
   printf("To see the items you are carrying, type '%s'.\n\n", COM_ITEMS);
   printf("To move to a new location, type '%s' or '%s' followed by an "
      "exit's name.  Possible move locations can be listed by typing "
      "'%s'.\n\n", COM_MOVE_0, COM_MOVE_1, COM_EXITS);

}

void printHint(int position, int items) {

   printf("Sorry, no availble hints right now.");
}

void printItems(int items, int clothes) {

   int i = TOTAL_OBJ - 1;
   int gameItems[TOTAL_OBJ] = {
      OBJ_BRACELET,
      OBJ_GOWN,
      OBJ_CLOTHES,
      OBJ_TOOLS,
      OBJ_MASK
   };
   
   while (i >= 0) {
      if (items & gameItems[i]){
         printPlayerHas(gameItems[i], clothes);
      }
      
      i--;
   
   }

}

void printPlayerHas(int item, int clothes){

   if (item == OBJ_BRACELET){
      //medical bracelet.  Always on.
      printf("You are wearing a plastic and metal bracelet. It won't come off. ");
   } else if (item == OBJ_GOWN) {
   
      printf("You have the %s", OBJ_GOWN_NAME);
   
      if (clothes == OBJ_GOWN) {
         printf(", and you are wearing it.");
      } else {
         printf(" that you were wearing when you woke up. ");
      }
   
   } else if (item == OBJ_CLOTHES) {
      
      printf("You have the %s. ", OBJ_CLOTHES_NAME);
      if (clothes == OBJ_CLOTHES){
         printf("You are wearing them. ");
      }
   
   } else {
      printf("You have some stuff that isn't described very well. ");
   }

}


void printExits(player the_player){
   if (the_player.position == LOC_CHAMBER){
      if (the_player.openPassages & PASS_CHAMBER_ROOM){
         printf("There is a room containing the %s.", 
            LOC_CHAMBER_NAME);
      } else {
         printf("There is a clearly a room beyond the glass of the %s.",
            LOC_CHAMBER_NAME);
      }
   }
}

int recognizeVerb(char * word, player the_player) {
   int result = FALSE;
   result = 
      (strcmp(word, COM_MOVE_0) == 0) ||
      (strcmp(word, COM_MOVE_1) == 0) ||
      (strcmp(word, COM_LOOK_0) == 0) ||
      (strcmp(word, COM_LOOK_1) == 0) ||
      (strcmp(word, COM_GET_0) == 0) ||
      (strcmp(word, COM_GET_1) == 0) ||
      recognizeRoomAction(word, the_player);
      
      
      
   return result;
}

int recognizeRoomAction(char * word, player the_player) {
   
}


void parseVerb(char * word, player the_player) {
   
}


