/*
 * fancy flag - print out the fanciest possible flag using only whiles
 * created by Seth Battin, December 10th 2012
 * 
 */
 
#include <stdlib.h>
#include <stdio.h>

#define ROW_COUNT 14
#define COL_COUNT 75
#define STAR_WIDTH 35
#define STAR_HEIGHT 7
#define IS_A_STAR 0
#define RED_STRIPE 177

 // there is only one function.  Skipped the prototyping.
void printUSwithPR(void){
 
   int row, col;
   char stripe = RED_STRIPE;

   row = 0;
   while (row <= ROW_COUNT){
      col = 0;
      printf("|");
      while (col <= COL_COUNT){
         if ((row == 0) || (row == (ROW_COUNT))){
            printf("-");
         } else if ((col <= STAR_WIDTH) && (row <= STAR_HEIGHT)){
            if (col == STAR_WIDTH){
               printf("|");
            } else if (row == STAR_HEIGHT){
               printf("-");
            } else if (((col + (2 * (row  % 2))) % 4) == 3){
               printf("*");
            } else {
               printf(" ");
            }
//         } else if (((row % 4) > 0) && ((row % 4) < 3)){
         } else if (row % 2 == 1) {
            printf("%c", stripe);
         }else {
            printf(" ");
         }
         col++;
      }
      printf("|\n");
      row++;
   }
   printf("                    United States (with Puerto Rico)\n\n");
   return; 
}

 
int main (int argc, char *argv[]){
 
   // run the function.  I'm not sure how this will be tested...
   printUSwithPR();
   
   return EXIT_SUCCESS;
 
}


