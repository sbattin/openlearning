/**
 * Author:      Seth Battin
 * Date:        2012-10-29
 * Program:     2bornot2b.c
 * Description: This program prints out a line from William Shakespeare's play
 *  Hamlet
 */

#include <stdio.h>

int main()
{
    //printf("Hello World!\n");   
    printf("To be, or not to be?\n");
    return (0);
}
