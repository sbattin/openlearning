/*
 *  freqanal.c
 *
 *  reads a stream fro mthe standard input and calculates the frequency
 *  of each letter in the stream.
 *
 *  Created by Seth Battin on 03/21/13.
 *
 */
 
#include <stdio.h>
#include <stdlib.h>
 
#define STOP -1
#define ALPHABET_SIZE 26
 
#define FIRST_LC_LETTER 'a'
#define LAST_LC_LETTER  'z'

#define FIRST_UC_LETTER 'A'
#define LAST_UC_LETTER 'Z'

int isLowerCase (char letter);
int isUpperCase (char letter);
 
int main (int argc, char *argv[]) {
 
   int counts[ALPHABET_SIZE];
   int total = 0;
   int i = 0;
   while (i < ALPHABET_SIZE){
      counts[i] = 0;
      i++;
   }
   
   char get = getchar();
   while (get != STOP) {
      if (isUpperCase(get)){
         counts[get - FIRST_UC_LETTER] ++;
         total++;
      } else if (isLowerCase(get)) {
         counts[get - FIRST_LC_LETTER] ++;
         total++;
      }
      get = getchar();
   }
   
   i = 0;
   while (i < ALPHABET_SIZE){
      printf("%c-%d/%d\n", (FIRST_LC_LETTER + i), counts[i], total);
      i++;
   }
   
   return EXIT_SUCCESS;
}
 
int isLowerCase (char letter) {
   return (letter >= FIRST_LC_LETTER) && (letter <= LAST_LC_LETTER);
}
int isUpperCase (char letter) {
   return (letter >= FIRST_UC_LETTER) && (letter <= LAST_UC_LETTER);
}

