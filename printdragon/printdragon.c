/**
 * Author:       Seth Battin
 * Date:         2012-12-29
 * Program:      printdragon.c
 * Description:  Prints out an ASCII dragon.
 */

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){

    //clear top space
    printf("\n\n");
    printf("                            _/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\__           __/\\/\\/\\________@\n");
    printf("                          __\\ v v  v  v  v  v  v  /_         _\\   0   vv\\/vv\\/\n");
    printf(" <^^^\\                   _\\ v  v v  v  v v v  v  v  /_     _\\                \n");
    printf("  ^^\\ \\                 _\\ v v  v v  v v v v  v v  v /_   _\\  v    /\\/^^^^^^^j\n");
    printf("     \\ \\               _\\ v v  v  _____________  v  v  /_\\      v /\n"); 
    printf("      \\  \\_          __\\  v  v  /              \\  v  v    v  v  /\n"); 
    printf("       \\_  \\________\\  v v  v /                  \\ v  v v  v  /\n");
    printf("         \\_  v        v  v  /                     \\  v  v   /\n");
    printf("           \\__ v v  v  v__/                         \\___/  /          ~\n");
    printf("                \\     \\                                /  /        _~ \n");
    printf("                 \\     \\                              {  <       _||\n");
    printf("                 _>    3___                            \\  \\     /   \\\n");
    printf("                <__---__vVV                             VVV     |  #|\n");
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

    printf("\n\n");
    return EXIT_SUCCESS;
}
