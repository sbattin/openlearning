
#include <stdio.h>
#include <stdlib.h>

#define CONSTANTA 3
#define CONSTANTB 4
#define CONSTANTC 7
#define B B*7
#define CONSTANTD CONSTANTA+CONSTANTB


int main(int argc, char *argv[]){

   // Do defines affect string literals?  I don't know that either
   printf("CONSTANTA = %d, CONSTANTB = %d\n", CONSTANTA, CONSTANTB);
   printf("CONSTANTA + CONSTANTB = %d\n", CONSTANTA+CONSTANTB);
   printf("CONSTANTC = %d\n", CONSTANTC);
   printf("CONSTANTD = %d\n", CONSTANTD);
   
   // all output appeared to be correct.  New tests!
   
   printf("CONSTANTC * 2 = %d\n", CONSTANTC * 2);
   printf("CONSTANTD * 2 = %d\n", CONSTANTD * 2);
   
   // OOPS! Different output!

   return EXIT_SUCCESS;

}
