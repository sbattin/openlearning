/*
 *  simpleEncode.c
 *
 *  reads in a permutation of the alphabet then encodes
 *  lower case letters using that permutation
 *
 *  Created by Julian Saknussemm on 01/04/11.
 *  Editted by Seth Battin on 03/20/13.
 *  Copyright 2011 Licensed under Creative Commons SA-BY-NC 3.0.
 *
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
 
#define STOP -1
#define ALPHABET_SIZE 26
 
#define FIRST_LC_LETTER 'a'
#define LAST_LC_LETTER  'z'

#define FIRST_UC_LETTER 'A'
#define LAST_UC_LETTER 'Z'
 
char encode (char plainChar, char *permuation);
void testEncode (void);
int main (int argc, char *argv[]) {
 
   //testEncode();
 
   char permutation[ALPHABET_SIZE];
   /*
   int i;
   char get;
   
   printf("Input 26 letters: ");

   i = 0;
   get = 0;
   
   // get only 26 inputs
   while (i < ALPHABET_SIZE) {
      
      get = getchar();
      if (get != STOP){
         
         //only store alphabet, and only as lower-case letters
         if (isUpperCase(get)){
            get = makeLowerCase(get);
            permutation[i] = get;
            i++;
         } else if (isLowerCase(get)){
            permutation[i] = get;
            i++;
         }   
      }
   }
   
   //disregard away any extra input
   while (get != STOP){
      get = getchar();
   }
   
   printf("\nDone inputting permutation string.\n");
   
   */ 
   scanf ("%s", permutation);
 
   // getchar() reads and returns one character from the keyboard
   // returns -1 when it reads end-of-file character (^D in linux)
   int plainCharacter = getchar();
   while (plainCharacter != STOP) {
      int encodedCharacter = encode (plainCharacter, permutation);
      printf ("%c", encodedCharacter);
      plainCharacter = getchar();
   }
   return EXIT_SUCCESS;
}
 
void testEncode (void) {
   assert (encode ('A',"abcdefghijklmnopqrstuvwxyz") == 'A');
   assert (encode ('?',"abcdefghijklmnopqrstuvwxyz") == '?');
   assert (encode (' ',"abcdefghijklmnopqrstuvwxyz") == ' ');
   assert (encode ('\n',"abcdefghijklmnopqrstuvwxyz") == '\n');
 
   assert (encode ('a',"abcdefghijklmnopqrstuvwxyz") == 'a');
   assert (encode ('m',"abcdefghijklmnopqrstuvwxyz") == 'm');
   assert (encode ('z',"abcdefghijklmnopqrstuvwxyz") == 'z');
 
   assert (encode ('a',"bcdefghijklmnopqrstuvwxyza") == 'b');
   assert (encode ('m',"bcdefghijklmnopqrstuvwxyza") == 'n');
   assert (encode ('z',"bcdefghijklmnopqrstuvwxyza") == 'a');
 
   assert (encode ('a',"qwertyuiopasdfghjklzxcvbnm") == 'q');
   assert (encode ('b',"qwertyuiopasdfghjklzxcvbnm") == 'w');
   assert (encode ('z',"qwertyuiopasdfghjklzxcvbnm") == 'm');
}


 
char encode (char plainChar, char *permutation) {
   
   char encoded;
   
   if ((plainChar >= FIRST_UC_LETTER) && (plainChar <= LAST_UC_LETTER)){
      encoded = (plainChar + (FIRST_LC_LETTER - FIRST_UC_LETTER));
      encoded = permutation[encoded - FIRST_LC_LETTER];
      encoded = (encoded - (FIRST_LC_LETTER - FIRST_UC_LETTER));
   } else if ((plainChar >= FIRST_LC_LETTER) && (plainChar <= LAST_LC_LETTER)){
      encoded = permutation[plainChar - FIRST_LC_LETTER];
   } else {
      encoded = plainChar;
   }
   
   return encoded;
}


