/*
 *  simpleEncode.c
 *
 *  reads in a permutation of the alphabet then encodes
 *  lower case letters using that permutation
 *
 *  Created by Julian Saknussemm on 01/04/11.
 *  Editted by Seth Battin on 03/20/13.
 *  Copyright 2011 Licensed under Creative Commons SA-BY-NC 3.0.
 *
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
 
#define STOP -1
#define ALPHABET_SIZE 26
 
#define FIRST_LC_LETTER 'a'
#define LAST_LC_LETTER  'z'

#define FIRST_UC_LETTER 'A'
#define LAST_UC_LETTER 'Z'
 
char encode (char plainChar, char *permuation);
void testEncode (void);
int isLowerCase (char letter);
int isUpperCase (char letter);
char makeLowerCase(char letter);
char makeUpperCase(char letter);
 
int main (int argc, char *argv[]) {
 
   //testEncode();
 
   char permutation[ALPHABET_SIZE];
   /*
   int i;
   char get;
   
   printf("Input 26 letters: ");

   i = 0;
   get = 0;
   
   // get only 26 inputs
   while (i < ALPHABET_SIZE) {
      
      get = getchar();
      if (get != STOP){
         
         //only store alphabet, and only as lower-case letters
         if (isUpperCase(get)){
            get = makeLowerCase(get);
            permutation[i] = get;
            i++;
         } else if (isLowerCase(get)){
            permutation[i] = get;
            i++;
         }   
      }
   }
   
   //disregard away any extra input
   while (get != STOP){
      get = getchar();
   }
   
   printf("\nDone inputting permutation string.\n");
   
   */ 
   scanf ("%s", permutation);
 
   // getchar() reads and returns one character from the keyboard
   // returns -1 when it reads end-of-file character (^D in linux)
   int plainCharacter = getchar();
   while (plainCharacter != STOP) {
      int encodedCharacter = encode (plainCharacter, permutation);
      printf ("%c", encodedCharacter);
      plainCharacter = getchar();
   }
   return EXIT_SUCCESS;
}
 
void testEncode (void) {
   assert (encode ('A',"abcdefghijklmnopqrstuvwxyz") == 'A');
   assert (encode ('?',"abcdefghijklmnopqrstuvwxyz") == '?');
   assert (encode (' ',"abcdefghijklmnopqrstuvwxyz") == ' ');
   assert (encode ('\n',"abcdefghijklmnopqrstuvwxyz") == '\n');
 
   assert (encode ('a',"abcdefghijklmnopqrstuvwxyz") == 'a');
   assert (encode ('m',"abcdefghijklmnopqrstuvwxyz") == 'm');
   assert (encode ('z',"abcdefghijklmnopqrstuvwxyz") == 'z');
 
   assert (encode ('a',"bcdefghijklmnopqrstuvwxyza") == 'b');
   assert (encode ('m',"bcdefghijklmnopqrstuvwxyza") == 'n');
   assert (encode ('z',"bcdefghijklmnopqrstuvwxyza") == 'a');
 
   assert (encode ('a',"qwertyuiopasdfghjklzxcvbnm") == 'q');
   assert (encode ('b',"qwertyuiopasdfghjklzxcvbnm") == 'w');
   assert (encode ('z',"qwertyuiopasdfghjklzxcvbnm") == 'm');
}


int isLowerCase (char letter) {
   return (letter >= FIRST_LC_LETTER) && (letter <= LAST_LC_LETTER);
}
int isUpperCase (char letter) {
   return (letter >= FIRST_UC_LETTER) && (letter <= LAST_UC_LETTER);
}

char makeLowerCase(char letter){

   char retVal;

   if (isUpperCase(letter)) {
      //shift value down by difference from ASCII upper to lower
      retVal = letter - (FIRST_LC_LETTER - FIRST_UC_LETTER);
   } else {
      retVal = letter;
   }

   return retVal;
   
}

char makeUpperCase(char letter){

   char retVal;

   if (isLowerCase(letter)) {
      //shift value up by the difference from aSCII lower to upper
      retVal = letter + (FIRST_LC_LETTER - FIRST_UC_LETTER);
   } else {
      retVal = letter;
   }

   return retVal;
}

 
char encode (char plainChar, char *permutation) {
   
   char encoded;
   
   if (isUpperCase(plainChar)){
      encoded = makeLowerCase(plainChar);
      encoded = permutation[encoded - FIRST_LC_LETTER];
      encoded = makeUpperCase(encoded);
   } else if (isLowerCase(plainChar)){
      encoded = permutation[plainChar - FIRST_LC_LETTER];
   } else {
      encoded = plainChar;
   }
   
   return encoded;
}


